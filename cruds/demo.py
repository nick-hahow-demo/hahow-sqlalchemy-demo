from models.users import User
from models.address import Address
from sqlalchemy.orm.session import Session


def create_user(session: Session):
    spongebob = User(
        name="spongebob",
        fullname="Spongebob Squarepants",
        addresses=[Address(email_address="spongebob@sqlalchemy.org")],
    )
    session.add(spongebob)
    session.commit()
    session.refresh(spongebob)

    return spongebob