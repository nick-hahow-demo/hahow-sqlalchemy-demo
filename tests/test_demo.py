from cruds.demo import create_user
from sqlalchemy.orm.session import Session


def test_create_user(sqlite_session: Session):
    user = create_user(session=sqlite_session)
    print(user)

    assert user.id is not None
